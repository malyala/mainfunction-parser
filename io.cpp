/*
Name : Input.cpp
Author: Amit Malyala, Copyright Amit Malyala 2016- Current.
Description:
Read input into a string.
Version: 0.1
*/
#include "Io.h"
#include <cstring>
#include <fstream>

/*
 Component Function: SCHAR8* io::getInput(void)
 Arguments:  None
 returns: None
 Description:
 Reads a ascii or UTF8 file into a vector.
 Version : 0.1
 Notes:
 Other functions should extract tokens from each char string returned by this function.
*/
SCHAR8* io::getInput(void)
{
    SCHAR8 ch = ' ';
    BOOL EndofInput = false;
    std::size_t Strsize=20, index=0;
    SCHAR8* charString =nullptr ;
    static bool Execonce=true;
    static std::size_t Count=0;
    static std::size_t charCount=0;
    static std::vector <SCHAR8> s;
    if (Execonce)
    {
        const SCHAR8* Fileptr="main.cpp";
        io::ReadFile(s,Fileptr);
        Execonce=false;
        Count=s.size();
    }
    if (charString!=nullptr)
    {
        delete[] charString;
        charString=nullptr;
    }
    if (charCount  <Count)
    {
        charString = new SCHAR8[Strsize];
        if (charString)
        {
            /*
            Read input until '\n' is detected.
            */
            while(EndofInput== false)
            {
                if (index==Strsize)
                {
                    // Resize charString
                    SCHAR8* AnotherString = new SCHAR8[Strsize+10];
                    if (AnotherString)
                    {
                        for (std::size_t i=0; i<Strsize; i++)
                        {
                            AnotherString[i]=charString[i];
                        }
                        Strsize+=10;
                        delete[] charString;
                        charString= AnotherString;
                    }
                    else
                    {
                        ErrorTracer(MEMORY_ALLOCATION_ERROR,0,0);
                        EndofInput=true;
                        charString=nullptr;
                    }
                }
                if (!EndofInput)
                {
                    ch=s[charCount];
                    if(ch == '\n')
                    {
                        //std::cout << "Entered new line " << std::endl;
                        // append new line char correctly
                        charString[index]='\n';
                        charString[index+1]='\0';
                        // End of input marker is '\n'
                        EndofInput =true;
                    }
                    else
                    {
                        charString[index]= ch;
                        index++;
                    }
                }
                charCount++;
            }
        }
        else
        {
            //charString=nullptr;
            ErrorTracer(MEMORY_ALLOCATION_ERROR,0,0);
        }
    }
    else
    {
  
        s.clear();
        Count=0;
        charCount=0;
    }
    //std::cout << "char string in getInput() " << charString << std::endl;
    return charString;
    
}

/*
 Component Function: void io::ReadFile(std::vector <SCHAR8> &s,const SCHAR8* Fileptr)
 Arguments:  Vector to process input and input file name.
 returns: None
 Description:
 Reads a ascii or UTF8 file into a vector.
 Version : 0.1
 Notes:
*/

void io::ReadFile(std::vector <SCHAR8> &s,const SCHAR8* Fileptr)
{
    std::string line;
    BOOL EndofInput = false;
    std::ifstream myfile (Fileptr);
    SCHAR8 c;
    if (myfile.is_open())
    {
        while (myfile.get(c ) && EndofInput== false)  // For reading bytes
        {
            /*
               Read input until EOF
            */
            if(c == EOF)
            {
                EndofInput = true;
            }
            else
            {
                s.push_back(c);
            }
        }
        myfile.close();
    }
    else
    {
        std::cout << std:: endl << "File not found";
    }
}
