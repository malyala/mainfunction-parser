/*
Name :  Parser.cpp
Author: Amit Malyala, Copyright Amit Malyala, 2019. All rights reserved.
Description:
This module initliazes parser
Notes:

Bug and Version history:
0.1 Initial version

Notes:
Coding Log:
01-11-19- Arithmetic expresssion evaluator with memory project taken as baseline. project compiles under iSO C++ 14 with settings
          -std=c++14 -Wall -Wpedantic  -fsanitize=leak
          Added :: Operator in DetectOpToken() and DetectOpString() and DetectOpType() functions.Added , operator
          Changed CurrentScopes raw pointer to  smart pointer unique_ptr
04-11-19 Created IO module and changed code to read characters from a file.  Added other character tokens and isKeyword() function in Lexer.
05-11-19 added error code for Missing ". Changed getTokenCharString() function in Lexer and getInput() function in IO.
06-11-19 Created application in parser. 

*/
#include "Parser.h"


//#if(0)
/* Comment to test module. Uncomment to integrate */
SINT32 main(void)
{
	App();

    return 0;
}
//#endif

/* App to get input char strings from source file. */
void App(void)
{
    BOOL endofInput=false;
    BOOL QuotesDetected=false;
    SCHAR8* CurrentString =nullptr;
    std::cout << "Initialize" <<std::endl;
    while (endofInput ==false)
    {
        CurrentString= Lexer::getTokencharstring();
        if (CurrentString)
        {
            //std::cout << "Current string is " << CurrentString << std::endl;
            if (strcmp(CurrentString,"\"") ==0)
            {
            	if (!QuotesDetected)
            	{
            		QuotesDetected=true;
					CurrentString= Lexer::getTokencharstring();
				}
				else
				{
					QuotesDetected=false;
				}
			}
			if (QuotesDetected)
			{
				//std::cout << CurrentString << std::endl;
				
			    UINT32 Length = DataStructures::strlength(CurrentString);
			    for (UINT32 i=0; i< Length;i++)
			    {
			    	if (CurrentString[i] != '\\' && CurrentString[i] != 'n')
			    	{
			    		std::cout << CurrentString[i]; 
					}
				}
				std::cout << std::endl;
			}
        }
        else
        {
            endofInput=true;
        }
    }
    std::cout << "Cleanup" << std::endl;
    
    
}


