/*
Name :  Parser.h
Author: Amit Malyala, Copyright Amit Malyala, 2019. All rights reserved.
Description:
This module intializes parser.
Notes:

Bug and revision history;
0.1 Initial version


*/

#pragma once 

#ifndef PARSER_H
#define PARSER_H

#include "SymbolTable.h"
#include "Lexer.h"
/* Test to get input lines from source file. */
void App(void);
#endif