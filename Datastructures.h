/*
Name :  DataStructures.h
Author: Amit Malyala , Copyright Amit Malyala 2016.
Date : 20-06-2016
Description:
Some data structures used for Abstract sytax tree, vector tables, string to number functions,
various search and detection fuctions.
Notes:
Bug and version history:
        Version 0.1 Initial version
                0.2 Added definition of Tree node which uses a double linked list.
*/

#ifndef DATASTRUCTURES_H
#define DATASTRUCTURES_H

#include <cstring>
#include <string>
#include "Std_types.h"
#include <iostream>
#include "Error.h"
#include "Lexer.h"


namespace DataStructures
{

/*
Component Function:  void InitDataStructures(void);
Arguments: None
returns : None
Version : 0.1
Description:
Initialize all vector tables and data structures, keyword list, symbol tables.
*/
void InitDataStructures(void);



/* Convert a string of Numbers to short integer */

/*
 Component Function: SINT32 stringtoint(const std::string & str)
 Arguments: a string which contains digits
 returns : string converted to integer
 Version : 0.1
 Description:
 Function to convert a string of Numbers into an integer.
*/

SINT32 stringtoint(const std::string &str);


/*
Component Function: std::string itostring(std::size_t a)
Arguments:  None
returns: None
Description:
Convert a decimal integer to std::string
Version : 0.1
*/
std::string itostring(const std::size_t& a);


/*
Component Function:  isAlphaString(const SCHAR8* str);
Arguments:  String to be searched in list of alphabets
Returns:
Description: This function looks up all characters of a string in a vector of alphabet chars.
Returns Alphabetic or not
Version : 0.1
*/
std::size_t isAlphaString(const SCHAR8* str);

/*
 Component std::size_t strlength(const SCHAR8 *str);
 Arguments: char string
 returns : decimal digit
 Version : 0.1
 Description:
 Returns length of a char string.
 */
std::size_t strlength(const SCHAR8 *str);
/*
 Component Function: BOOL isAlphabet(SCHAR8 Character)
 Arguments:  Character to be searched in list of alphabets
 Returns:
 Description: This function looks up all characters of a string in a list of alphabets a-z and A-Z
 Returns Alphabetic or not
 Optional: Write a regex algorithm to detect alphabetical characters in a string.
 Version : 0.1
 */
BOOL isAlphabet(SCHAR8 Character);


/*
Component Function: BOOL isNumeric(const SCHAR8* Str)
Arguments:  String character to be searched in list of digits
Returns:
Description: This function looks up all characters of a string in a list of digits and returns the nature
of the number whether its a decimal,floating point or double precision number.
returns a number or not
Version : 0.2
*/
BOOL isNumeric(const SCHAR8* str);

/*
 Component Function: ULONGLONG DecMultiplier(std::size_t index)
 Arguments: a string which contains digits
 returns : a multiplier of pow(10,n)
 Version : 0.1
 Description:
  To do:
 Use a loop to convert float digits upto 20 decimal points and 20 floating points.
 */
ULONGLONG DecMultiplier(std::size_t index);

/*
 Component Function: FLOAT64 tofloat(const SCHAR8* str)
 Arguments: a string which contains digits
 returns : string converted to floating point
 Version : 0.1
 Description:
 Function to convert a string of Number into an Floating point number.
 Get the Number of digits entered as a string.
 Recognize a . in the string and put the decimal part before the . and the mantissa after the dot.
 Notes:

 This function would be used by parser/lexer module.

 To do:
 Use a loop to convert float digits upto 20 decimal points and 20 floating points.
 */
FLOAT64 atodouble(const SCHAR8* str);

/*
 Component FLOAT32 MantissaFloatMultiplier(std::size_t index)
 Arguments: Position of Mantissa digit
 returns : a multiplier of pow(0.1,n)
 Version : 0.1
 Description:
 Returns pow (0.1,index)
 */
FLOAT32 MantissaFloatMultiplier(std::size_t index);

/*
 Component FLOAT64 MantissaDoubleMultiplier(std::size_t index)
 Arguments: Position of Mantissa digit
 returns : a multiplier of pow(0.1,n)
 Version : 0.1
 Description:
 Returns pow (0.1,index)
 */
FLOAT64 MantissaDoubleMultiplier(std::size_t index);





/*
 Component std::size_t strlength(SCHAR8 *str)
 Arguments: char string
 returns : Length of a string
 Version : 0.1
 Description:
 Returns length of a char string.
 */
std::size_t strlength(SCHAR8 *str);

/*
 Component Function: BOOL isDigit(SCHAR8 Digit)
 Arguments: Digit
 returns : Digit or not
 Version : 0.1
 Description:
 Notes:
 */
BOOL isDigit(SCHAR8 Digit);
}

#endif
