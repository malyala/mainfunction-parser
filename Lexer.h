/*
Name :  Lexer.h
Author: Amit Malyala, Copyright Amit Malyala, 2019. All rights reserved.
Description:
This module creates lexer tokens.
Notes:
Bug and revision history;
0.1 Initial version
*/
#ifndef LEXER_H
#define LEXER_H

#include "std_types.h"
#include <cstddef>
#include "DataStructures.h"
#include "io.h"
#include "symboltable.h"


/* Type Numeric Tokens */
#define LONGDOUBLETYPE 0
#define DOUBLETYPE 1
#define FLOATTYPE 2
#define UINTTYPE 3
#define INTTYPE 4
#define LONGINTTYPE 5
#define LONGLONGINTTYPE 6
#define ULONGINTTYPE 7
#define SHORTTYPE 8
#define USHORTTYPE 9
#define UCHARTYPE 10
#define CHARTYPE 11
#define BOOLTYPE 12
#define CSTRINGTYPE 13
#define NOTADATATYPE 14



/* Other tokens */
#define HASHTYPE 15
#define LEFTPARENTHESIS 16
#define RIGHTPARENTHESIS 17
#define ENDOFINPUT 18
#define NOT_A_NUMBER 19
#define NEWLINETYPE 21
#define IDENT 22
#define NOTSTRING 34


/* List of operators in decreasing order of precedence..
To do:
Instead of giving numers to predence, use some type of string look up in a data structure to find a operator's precedence.
*/
// Change op precedence numbers. 

#define OPSCOPERESOLLUTION 0 // :: operator 
#define OPFUNCTION 1
#define OPSIN 1
#define OPCOS 1
#define OPTAN 1
#define OPCOSEC 1
#define OPSEC 1
#define OPCOT 1
#define OPSQRT 1
#define OPLOG 1
#define OPPOWEXPONENT 1
#define OPUNARYMINUS 2
/* Check precedence of op unary plus */
#define OPUNARYPLUS 3
#define OPCOMPLEMENT 3
#define OPLOGICALNOT 4
#define OPMUL 5
#define OPDIV 5
#define OPMODULUS 5
#define OPPLUS 8
#define OPMINUS 8
#define OPLEFTBITSHIFT 10
#define OPRIGHTBITSHIFT 10
#define OPLESSTHAN 12
#define OPLESSTHANEQUALTO 12
#define OPGREATERTHAN 12
#define OPGREATERTHANEQUALTO 12
#define OPLOGICALISEQUALTO 16
#define OPLOGICALISNOTEQUALTO 16
#define OPBITWISEAND 18
#define OPBITWISEXOR 19
#define OPBITWISEOR 20
#define OPLOGICALAND 21
#define OPLOGICALOR 22
#define OPCOMPLEMENTEQUALTO 23
#define OPOREQUALTO 24
#define OPXOREQUALTO 25
#define OPANDEQUALTO 26
#define OPRIGHTSHIFTEQUALTO 27
#define OPLEFTSHIFTEQUALTO 28
#define OPMODEQUALTO 29
#define OPDIVEQUALTO 30
#define OPMULTIPLYEQUALTO 31
#define OPMINUSEQUALTO 32
#define OPADDEQUALTO 33
#define OPEQUALTO 34
#define OPCOMMA  35
#define NOTOP 36

/* Lexer functions and token types */
namespace Lexer
{

/* Token types */
enum class TokenType
{
    identifier,
    separator_leftParen,
    separator_rightParen,
    op,
    numericliteral,
    command,
    endofline,
    lineterminator, // for ; 
    beginbrace,
    endbrace,
    namespacecpp,
    error,
    quotes,
    string,
    keyword,
    namelessobject,
    eof,
    Emptytoken
};


/* Token definition */
class Token_tag
{
public:
    SCHAR8* name;
    std::size_t LineNumber;
    std::size_t ColumnNumber;
    TokenType Type;
    Variant Data;
};




/* Peek next char in input */

/*
Component function: SCHAR8 PeekNextChar(SCHAR8* str, std::size_t Length, std::size_t index)
Arguments:  char string, length, current index.
Returns: next char of index.
Description:  returns a char
Version : 0.1
*/

SCHAR8 PeekNextChar(SCHAR8* str, std::size_t Length, std::size_t index);


/* Skip whitespace */
void SkipWhitespace(SCHAR8* str, std::size_t Length, std::size_t& i);


/*
 Component Function: BOOL IsSeparatorr(SCHAR8 Character)
 Arguments:  Character to be searched in separators
 Returns:
 Description:
 Checks if a char is a separator.
  Version : 0.1
 */
BOOL IsSeparator(SCHAR8 Character);
/*
Component Function: BOOL isOperchar(SCHAR8 Ch)
Arguments:  character
Returns:
Description:
returns char is a operator or not
 Version : 0.1
*/
BOOL isOperchar(SCHAR8 Ch);

/*
 Component Function: BOOL  isIdent(const SCHAR8* str)
 Arguments:  String characters to be searched in list of alphabets and _
 Returns:
 Description: This function looks up all characters of a string in a list of alphabets a-z and A-Z and digits
 Returns Alphabetic or not
 Optional: Write a regex algorithm to detect alphabetical characters in a string.
 Version : 0.1
 */
BOOL isIdent(const SCHAR8* str);

/*
 Component Function: BOOL isIdentChar(SCHAR8 Character)
 Arguments:  Character to be searched in list of alphabets and characters
 Returns:
 Description: This function looks up all characters of a string in a list of alphabets a-z and A-Z
 Returns Alphabetic or not
  Version : 0.1
 */
BOOL isIdentChar(SCHAR8 Character);
/*
 Component function: SCHAR8* getTokencharstring(void)
 Arguments: char string
 returns : a substring of type SCHAR8*
 Version : 0.1
 Description:
 parses a char string into substrings.
 Each substring canbe these:
 ident,
 numeric,
 operator
 parenthesis ( )
 These substrings would be read by Tokenprocessor() function.

 Notes:
 Function being edited

 */
SCHAR8* getTokencharstring(void);
/*
Component function: SCHAR8 PeekNextChar(SCHAR8* str, std::size_t Length, std::size_t index)
Arguments:  char string, length, current index.
Returns: next char of index.
Description:  returns a char
Version : 0.1
*/
SCHAR8 PeekNextChar(SCHAR8* str, std::size_t Length, std::size_t index);


/* define a char for end of input. */
constexpr SCHAR8 ENDOFINPUTSTRING = '#';

/*
 Component Function: BOOL isblockSeparator(SCHAR8 Character)
 Arguments:  Character to be searched in separators
 Returns:
 Description:
 Checks if a char is a separator.
  Version : 0.1
 */
BOOL isblockSeparator(SCHAR8 Character);

/*
 Component Function: BOOL isWhitespace(SCHAR8 Character)
 Arguments:  Character to be checked
 Returns:
 Description:
 This function checks if a character is whitespace or not
  Version : 0.1
 */
BOOL isWhitespace(SCHAR8 Character);


/*
 Component Function: BOOL isSeparator(SCHAR8 Character)
 Arguments:  Character to be searched in separators
 Returns:
 Description:
 Checks if a char is a separator.
  Version : 0.1
 */
BOOL isSeparator(SCHAR8 Character);





/*
 Component Function: BOOL isOperator(const SCHAR8* str)
 Arguments:  string to be checked.
 Returns:
 Description: This function checks if char string is a operator.
 Returns ident or not
 Version : 0.1
 */
BOOL isOperator(const SCHAR8* str);
/*
 Component Function: std::size_t DetectOpType(SCHAR8 const* OpString)
 Arguments:  char string
 returns: None
 Description:
 Detects Op type
 Version : 0.1
 Notes:
 */
std::size_t DetectOpType(SCHAR8 const* OpString);



/*
 Component Function: BOOL isValidOp(std::size_t OpType)
 Arguments:  Op type
 returns: None
 Description:
 Detects Op type
 Version : 0.1
 Notes:
 */
BOOL isValidOp(std::size_t OpType);

/*
 Component Function: void PrintToken(const Lexer::Token_tag& Token)
 Arguments:
 Returns:
 Description:
  Prints a token.
  Version : 0.1
 */
void PrintToken(const Lexer::Token_tag& Token);


/*
 Component Function: void ClearToken(Lexer::Token_tag& Token)
 Arguments:
 Returns:
 Description:
  Prints a token.
  Version : 0.1
 */
void ClearToken(Lexer::Token_tag& Token);

/*
 Component Function: SCHAR8 const* DetectOpstring(  const Lexer::Token_tag& OpToken)
 Arguments:  Token
 returns: Op string
 Description:
 Detects Op type
 Version : 0.1
 Notes:
 */
SCHAR8 const* DetectOpstring(const Lexer::Token_tag& OpToken);


}



#endif // #ifndef LEXER_H