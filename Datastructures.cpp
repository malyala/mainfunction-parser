/*
 Name :  DataStructures.cpp
 Author: Amit Malyala , copyright 2016-Current Amit Malyala. All rights reserved.
 Description:
 This module has API functions which are used by Lexer, Parser and AST and evaluator modules.

 Notes:

 To do:
 Convert all numeric types to double.


 Bug and version history:
 0.01 Initial version adapted from Expression evaluator.

 Known issues:
 None

 Unknown issues:

 typedef signed short SINT16; gives a SINT16 does not name a type compilation error with option std=C++11.
 When the type SINT16 is used with std=c++14 , there are no errors.

 */

#include "DataStructures.h"
#include "cmath"
#include <iomanip>
#include "stdio.h"


namespace local_DS
{

/*
 Component Function: static std::size_t getNumofDecDigit(std::size_t Number)
 Arguments:  None
 returns: Number of digits
 Description:
 Returns Number of digits in a integer
 Version : 0.1
 */
static inline std::size_t getNumofDecDigit(std::size_t Number);

/*
 Component static std::size_t getDecDigit(const UCHAR8& str)
 Arguments: Chat digit
 returns : decimal digit
 Version : 0.1
 Description:
 Converts a char to decimal digit.
 */
static std::size_t getDecDigit(const UCHAR8& str);

/*
 Component Function: static UCHAR8 getStrChar(std::size_t digit)
 Arguments:  None
 returns: a digit as a char
 Description:
 Get a character from  digit input number
 Version : 0.1
 */
static UCHAR8 getStrChar(const std::size_t& digit);

} /* inline namespace local_DS */

// Comment when module is integrated with the project
// Uncomment to develop or test
#if(0)
SINT32 main(void)
{
    InitDataStructures();

    //std::cout << "\nNum is " << num;
    return ZERO;
}
#endif

/*
 Component Function: void DataStructures::InitDataStructures(void);
 Arguments: None
 returns : None
 Version : 0.1
 Description:
 Initialize all vector tables and data structures, keyword list, symbol tables.
 */
void DataStructures::InitDataStructures(void)
{



}

/*
 Component Function: BOOL DataStructures::isDigit(SCHAR8 Digit)
 Arguments: Digit
 returns : Digit or not
 Version : 0.1
 Description:
 Notes:
 */
BOOL DataStructures::isDigit(SCHAR8 Digit)
{
    if (Digit >= '0' && Digit <= '9')
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
 Component Function: SINT32 DataStructures::stringtoint(const std::string & str)
 Arguments: a string which contains digits
 returns : string converted to integer
 Version : 0.1
 Description:
 Function to convert a string of Numbers into an integer.
 Get the Number of digits entered as a string.

 For each digit, place it in appropriate place of an integer such as
 digit x 1000 or digit x10000 depending on integer size and input range.
 The multiple of the the first and subsequent digits would be selected depending
 on the Number of digits.

 For example: 123 would be calculated as an integer in the following steps:

 1* 100
 2* 10
 3* 1

 The calculated value is then returned by the function.

 For example, if the digits entered are 12345
 Then, the multipliers are:
 str[0] * 10000
 str[1] * 1000
 str[2] * 100
 str[3] * 10
 str[4] * 1
 Notes:
 Optional: Convert this function use std::string instead of const char * c-style string
 This function would be used by parser module.
 */

SINT32 DataStructures::stringtoint(const std::string &str)
{
    SINT32 strlen = str.length();
    SINT32 Number = 0;
    if (str[0] != '-')
    {
        switch (strlen)
        {
        case 0:
            Number += local_DS::getDecDigit(str[0]) * 0;
            break;

        // Convert characters to digits with another function.
        case 1:
            Number += local_DS::getDecDigit(str[0]) * 1;
            break;

        case 2:

            Number += local_DS::getDecDigit(str[0]) * 10;
            Number += local_DS::getDecDigit(str[1]) * 1;
            break;

        case 3:

            Number += local_DS::getDecDigit(str[0]) * 100;
            Number += local_DS::getDecDigit(str[1]) * 10;
            Number += local_DS::getDecDigit(str[2]) * 1;
            break;

        case 4:
            Number += local_DS::getDecDigit(str[0]) * 1000;
            Number += local_DS::getDecDigit(str[1]) * 100;
            Number += local_DS::getDecDigit(str[2]) * 10;
            Number += local_DS::getDecDigit(str[3]) * 1;
            break;

        case 5:
            Number += local_DS::getDecDigit(str[0]) * 10000;
            Number += local_DS::getDecDigit(str[1]) * 1000;
            Number += local_DS::getDecDigit(str[2]) * 100;
            Number += local_DS::getDecDigit(str[3]) * 10;
            Number += local_DS::getDecDigit(str[4]) * 1;
            break;

        case 6:
            Number += local_DS::getDecDigit(str[0]) * 100000;
            Number += local_DS::getDecDigit(str[1]) * 10000;
            Number += local_DS::getDecDigit(str[2]) * 1000;
            Number += local_DS::getDecDigit(str[3]) * 100;
            Number += local_DS::getDecDigit(str[4]) * 10;
            Number += local_DS::getDecDigit(str[5]) * 1;
            break;

        case 7:
            Number += local_DS::getDecDigit(str[0]) * 1000000;
            Number += local_DS::getDecDigit(str[1]) * 100000;
            Number += local_DS::getDecDigit(str[2]) * 10000;
            Number += local_DS::getDecDigit(str[3]) * 1000;
            Number += local_DS::getDecDigit(str[4]) * 100;
            Number += local_DS::getDecDigit(str[5]) * 10;
            Number += local_DS::getDecDigit(str[6]) * 1;
            break;

        case 8:
            Number += local_DS::getDecDigit(str[0]) * 10000000;
            Number += local_DS::getDecDigit(str[1]) * 1000000;
            Number += local_DS::getDecDigit(str[2]) * 100000;
            Number += local_DS::getDecDigit(str[3]) * 10000;
            Number += local_DS::getDecDigit(str[4]) * 1000;
            Number += local_DS::getDecDigit(str[5]) * 100;
            Number += local_DS::getDecDigit(str[6]) * 10;
            Number += local_DS::getDecDigit(str[7]) * 1;
            break;

        /* Reserved for future implementation */
        /* Define custom data types to hold numeric data
         with 64 and 128 bits if machine architecture supports it.*/

        case 9:
            Number += local_DS::getDecDigit(str[0]) * 100000000;
            Number += local_DS::getDecDigit(str[1]) * 10000000;
            Number += local_DS::getDecDigit(str[2]) * 1000000;
            Number += local_DS::getDecDigit(str[3]) * 100000;
            Number += local_DS::getDecDigit(str[4]) * 10000;
            Number += local_DS::getDecDigit(str[5]) * 1000;
            Number += local_DS::getDecDigit(str[6]) * 100;
            Number += local_DS::getDecDigit(str[7]) * 10;
            Number += local_DS::getDecDigit(str[8]) * 1;
            break;
        case 10:
            Number += local_DS::getDecDigit(str[0]) * 1000000000;
            Number += local_DS::getDecDigit(str[1]) * 100000000;
            Number += local_DS::getDecDigit(str[2]) * 10000000;
            Number += local_DS::getDecDigit(str[3]) * 1000000;
            Number += local_DS::getDecDigit(str[4]) * 100000;
            Number += local_DS::getDecDigit(str[5]) * 10000;
            Number += local_DS::getDecDigit(str[6]) * 1000;
            Number += local_DS::getDecDigit(str[7]) * 100;
            Number += local_DS::getDecDigit(str[8]) * 10;
            Number += local_DS::getDecDigit(str[9]) * 1;
            break;


        default:
            Number = 0;
            break;
        }

    }
    else if ((str[0] == '-') || str[0] == '+')
    {
        strlen--;
        switch (strlen)
        {
        case 0:
            Number += local_DS::getDecDigit(str[0]) * 0;
            break;

        // Convert characters to digits with another function.
        case 1:
            Number += local_DS::getDecDigit(str[1]) * 1;
            break;

        case 2:

            Number += local_DS::getDecDigit(str[1]) * 10;
            Number += local_DS::getDecDigit(str[2]) * 1;
            break;

        case 3:

            Number += local_DS::getDecDigit(str[1]) * 100;
            Number += local_DS::getDecDigit(str[2]) * 10;
            Number += local_DS::getDecDigit(str[3]) * 1;

            break;

        case 4:
            Number += local_DS::getDecDigit(str[1]) * 1000;
            Number += local_DS::getDecDigit(str[2]) * 100;
            Number += local_DS::getDecDigit(str[3]) * 10;
            Number += local_DS::getDecDigit(str[4]) * 1;

            break;

        case 5:
            Number += local_DS::getDecDigit(str[1]) * 10000;
            Number += local_DS::getDecDigit(str[2]) * 1000;
            Number += local_DS::getDecDigit(str[3]) * 100;
            Number += local_DS::getDecDigit(str[4]) * 10;
            Number += local_DS::getDecDigit(str[5]) * 1;
            break;

        case 6:
            Number += local_DS::getDecDigit(str[1]) * 100000;
            Number += local_DS::getDecDigit(str[2]) * 10000;
            Number += local_DS::getDecDigit(str[3]) * 1000;
            Number += local_DS::getDecDigit(str[4]) * 100;
            Number += local_DS::getDecDigit(str[5]) * 10;
            Number += local_DS::getDecDigit(str[6]) * 1;
            break;

        case 7:
            Number += local_DS::getDecDigit(str[1]) * 1000000;
            Number += local_DS::getDecDigit(str[2]) * 100000;
            Number += local_DS::getDecDigit(str[3]) * 10000;
            Number += local_DS::getDecDigit(str[4]) * 1000;
            Number += local_DS::getDecDigit(str[5]) * 100;
            Number += local_DS::getDecDigit(str[6]) * 10;
            Number += local_DS::getDecDigit(str[7]) * 1;
            break;

        case 8:
            Number += local_DS::getDecDigit(str[1]) * 10000000;
            Number += local_DS::getDecDigit(str[2]) * 1000000;
            Number += local_DS::getDecDigit(str[3]) * 100000;
            Number += local_DS::getDecDigit(str[4]) * 10000;
            Number += local_DS::getDecDigit(str[5]) * 1000;
            Number += local_DS::getDecDigit(str[6]) * 100;
            Number += local_DS::getDecDigit(str[7]) * 10;
            Number += local_DS::getDecDigit(str[8]) * 1;
            break;

        /* Reserved for future implementation */
        /* Define custom data types to hold numeric data
         with 64 and 128 bits if machine architecture supports it.*/

        case 9:
            Number += local_DS::getDecDigit(str[1]) * 100000000;
            Number += local_DS::getDecDigit(str[2]) * 10000000;
            Number += local_DS::getDecDigit(str[3]) * 1000000;
            Number += local_DS::getDecDigit(str[4]) * 100000;
            Number += local_DS::getDecDigit(str[5]) * 10000;
            Number += local_DS::getDecDigit(str[6]) * 1000;
            Number += local_DS::getDecDigit(str[7]) * 100;
            Number += local_DS::getDecDigit(str[8]) * 10;
            Number += local_DS::getDecDigit(str[9]) * 1;
            break;
        case 10:
            Number += local_DS::getDecDigit(str[0]) * 1000000000;
            Number += local_DS::getDecDigit(str[1]) * 100000000;
            Number += local_DS::getDecDigit(str[2]) * 10000000;
            Number += local_DS::getDecDigit(str[3]) * 1000000;
            Number += local_DS::getDecDigit(str[4]) * 100000;
            Number += local_DS::getDecDigit(str[5]) * 10000;
            Number += local_DS::getDecDigit(str[6]) * 1000;
            Number += local_DS::getDecDigit(str[7]) * 100;
            Number += local_DS::getDecDigit(str[8]) * 10;
            Number += local_DS::getDecDigit(str[9]) * 1;
            break;

        default:
            Number = 0;
            break;
        }

        Number = (str[0] == '-') ? -Number : Number;
    }

    return Number;
}


/*
 Component function: std::size_t DataStructures::strlength(const SCHAR8 *str);
 Arguments: char string
 returns : Length of a string
 Version : 0.1
 Description:
 Returns length of a char string.
 */
std::size_t DataStructures::strlength(const SCHAR8 *str)
{
    std::size_t count = 0;
    while (str[count] != '\0')
    {
        count++;
    }
    return count;
}


/*
 Component std::size_t DataStructures::strlength(SCHAR8 *str)
 Arguments: char string
 returns : Length of a string
 Version : 0.1
 Description:
 Returns length of a char string.
 */
std::size_t DataStructures::strlength(SCHAR8 *str)
{
    std::size_t count = 0;
    while (str[count] != '\0')
    {
        count++;
    }
    return count;
}


// get a digit from corresponding character in input string

/*
 Component static std::size_t local_DS::getDecDigit(const UCHAR8& str)
 Arguments: Chat digit
 returns : decimal digit
 Version : 0.1
 Description:
 Converts a char to decimal digit.
 */
static std::size_t local_DS::getDecDigit(const UCHAR8& str)
{
    std::size_t digit = 0;
    switch (str)
    {
    case '0':
        digit = 0;
        break;

    case '1':
        digit = 1;
        break;

    case '2':
        digit = 2;
        break;

    case '3':
        digit = 3;
        break;

    case '4':
        digit = 4;
        break;

    case '5':
        digit = 5;
        break;

    case '6':
        digit = 6;
        break;

    case '7':
        digit = 7;
        break;

    case '8':
        digit = 8;
        break;

    case '9':
        digit = 9;
        break;

    default:
        digit = 0;
        break;
    }
    return digit;
}

/*
 Component Function: static std::string DataStructures::itostring(std::size_t a)
 Arguments:  std::size_t to be converted to std::string
 returns: None
 Description:
 Convert a decimal unsigned integer to std::string
 License: GPL V3
  Version : 0.1
To do:
itostring should have line number information.

 */
std::string DataStructures::itostring(const std::size_t& a)
{
    std::string b;
    std::size_t NumofDigits = local_DS::getNumofDecDigit(a);

    switch (NumofDigits)
    {
    case 0:
        break;

    case 1:
        b += local_DS::getStrChar(a);

        break;

    case 2:
        b += local_DS::getStrChar(a / 10);
        b += local_DS::getStrChar(a % 10);

        break;

    case 3:
        b += local_DS::getStrChar(a / 100);
        b += local_DS::getStrChar((a / 10) % 10);
        b += local_DS::getStrChar(a % 10);

        break;

    case 4:
        b += local_DS::getStrChar(a / 1000);
        b += local_DS::getStrChar((a / 100) % 10);
        b += local_DS::getStrChar((a / 10) % 10);
        b += local_DS::getStrChar(a % 10);

        break;

    case 5:
        b += local_DS::getStrChar(a / 10000);
        b += local_DS::getStrChar((a / 1000) % 10);
        b += local_DS::getStrChar((a / 100) % 10);
        b += local_DS::getStrChar((a / 10) % 10);
        b += local_DS::getStrChar(a % 10);

        break;

    case 6:
        b += local_DS::getStrChar(a / 100000);
        b += local_DS::getStrChar((a / 10000) % 10);
        b += local_DS::getStrChar((a / 1000) % 10);
        b += local_DS::getStrChar((a / 100) % 10);
        b += local_DS::getStrChar((a / 10) % 10);
        b += local_DS::getStrChar(a % 10);

        break;

    case 7:
        b += local_DS::getStrChar(a / 1000000);
        b += local_DS::getStrChar((a / 100000) % 10);
        b += local_DS::getStrChar((a / 10000) % 10);
        b += local_DS::getStrChar((a / 1000) % 10);
        b += local_DS::getStrChar((a / 100) % 10);
        b += local_DS::getStrChar((a / 10) % 10);
        b += local_DS::getStrChar(a % 10);

        break;

    case 8:
        b += local_DS::getStrChar(a / 10000000);
        b += local_DS::getStrChar((a / 1000000) % 10);
        b += local_DS::getStrChar((a / 100000) % 10);
        b += local_DS::getStrChar((a / 10000) % 10);
        b += local_DS::getStrChar((a / 1000) % 10);
        b += local_DS::getStrChar((a / 100) % 10);
        b += local_DS::getStrChar((a / 10) % 10);
        b += local_DS::getStrChar(a % 10);
        break;

    default:
        ErrorTracer(UNABLE_TO_PROCESS_LINENUMBER, 10, 1);
        break;
    }
    return b;
}

/*
 Component Function: static std::size_t local_DS::getNumofDecDigit(std::size_t Number)
 Arguments:  Number to detect digits
 returns: Number of digits
 Description:
 Returns number of digits in a std::size_teger
 Version : 0.1
 */

static inline std::size_t local_DS::getNumofDecDigit(std::size_t Number)
{
    std::size_t NumofDigits = 0;
    //Number= (Number>0)? Number: -Number; // This is always true for unsigned integers
    while (Number)
    {
        Number /= 10;
        NumofDigits++;
    }
    return NumofDigits;
}

/*
 Component Function: static UCHAR8 local_DS::getStrChar(std::size_t digit)
 Arguments:  digit as std::size_t
 Returns: a digit as a char
 Description:
 Get a character from  digit input number
 Version : 0.1
 */
static UCHAR8 local_DS::getStrChar(const std::size_t& digit)
{
    UCHAR8 StrChar = ' ';
    switch (digit)
    {
    case 0:
        StrChar = '0';
        break;
    case 1:
        StrChar = '1';
        break;
    case 2:
        StrChar = '2';
        break;
    case 3:
        StrChar = '3';
        break;
    case 4:
        StrChar = '4';
        break;
    case 5:
        StrChar = '5';
        break;
    case 6:
        StrChar = '6';
        break;
    case 7:
        StrChar = '7';
        break;
    case 8:
        StrChar = '8';
        break;
    case 9:
        StrChar = '9';
        break;
    default:
        StrChar = '0';
        break;
    }
    return StrChar;
}



/*
 Component Function: BOOL DataStructures::isAlphabet(SCHAR8 Character)
 Arguments:  Character to be searched in list of alphabets
 Returns:
 Description: This function looks up all characters of a string in a list of alphabets a-z and A-Z
 Returns Alphabetic or not
  Version : 0.1
 */
BOOL DataStructures::isAlphabet(SCHAR8 Character)
{
    return ((Character >= 'a' && Character <= 'z') || (Character >= 'A' && Character <= 'Z'));
}


/*
 Component Function: DataStructures::FLOAT64 todouble(const SCHAR8* str)
 Arguments: a string which contains digits
 returns : string converted to double number
 Version : 0.1
 Description:
 Function to convert a string of Number into an Floating point number.
 Get the Number of digits entered as a string.
 Recognize a . in the string and put the decimal part before the . and the mantissa after the dot.
 Notes:
  */
FLOAT64 DataStructures::atodouble(const SCHAR8* str)
{
    std::size_t index = ZERO;
    FLOAT64 Number = ZERO;
    FLOAT64 Characteristic = ZERO;
    FLOAT64 Mantissa = ZERO;
    std::size_t DecDigit = ZERO;
    std::size_t DecCount = ZERO;
    std::size_t FloatPosition = ZERO;
    std::size_t FloatIndex = ZERO;
    std::size_t size = DataStructures::strlength(str);
    while (str[DecCount] != '.' && str[DecCount] != '\0')
    {
        DecCount++;
    }
    // Get the floating point part
    FloatPosition = DecCount + 1;
    // For loop to calculate all characteristic and mantissa digits.
    for (index = 0; index < size; index++)
    {

        if (index < DecCount)
        {
            DecDigit = DecCount - index - 1;
            Characteristic += static_cast<FLOAT64> (local_DS::getDecDigit(str[DecDigit]) * DecMultiplier(index));
        }
        else
        {
            if (index == DecCount)
            {
                index++;
                FloatIndex = 1;
            }
            if (FloatPosition != size)
            {
                Mantissa += static_cast<FLOAT64> (local_DS::getDecDigit(str[FloatPosition]) * MantissaDoubleMultiplier(FloatIndex));
            }
            else
            {
                Mantissa = 0;
            }
            FloatPosition++;
            FloatIndex++;
        }

    }
    Number = Characteristic + Mantissa;
    return Number;
}

/*
 Component Function: ULONGLONG DataStructures::DecMultiplier(std::size_t index)
 Arguments: a string which contains digits
 returns : a multiplier of pow(10,n)
 Version : 0.1
 Description:
  To do:
 Use a loop to convert float digits upto 20 decimal points .
 */
ULONGLONG DataStructures::DecMultiplier(std::size_t index)
{
    FLOAT64 ReturnValue = 0;
    std::size_t TEN = 10;
    ReturnValue = pow(TEN, index);
    return static_cast <ULONGLONG>  (ReturnValue );
}


/*
Component FLOAT64 DataStructures::MantissaDoubleMultiplier(std::size_t index)
Arguments: Position of Mantissa digit
returns : a multiplier of pow(0.1,n)
Version : 0.1
Description:
Returns pow (0.1,index)
 */
FLOAT64 DataStructures::MantissaDoubleMultiplier(std::size_t index)
{
    FLOAT64 ReturnValue = 0.0;
    FLOAT64 Mantissa = 0.1;
    ReturnValue = pow(Mantissa, index);
    return  (ReturnValue );
}



/*
 Component Function: BOOL DataStructures::isNumeric(const SCHAR8* Str)
 Arguments:  String character to be searched in list of digits
 Returns:
 Description: This function looks up all characters of a string in a list of digits and returns the nature of the number whether
 it is a numeric type or not.
 Notes:
 Check this function for correctness.

  Add support for hexa and octal numbers.
 Version : 0.2
 */
BOOL DataStructures::isNumeric(const SCHAR8* str)
{

    std::size_t index = 0;
    std::size_t StrLength = DataStructures::strlength(str);
    std::size_t DecimalCount = 0;
    BOOL isNum = false;
    std::size_t NumberType = NOT_A_NUMBER;
    std::size_t FloatingDigitCount = 0;
    std::size_t PeriodCount=0;
    std::size_t OtherChar=0;
    while (index < StrLength)
    {
        switch (str[index])
        {
        // Reading alphabetical chars
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            if (NumberType == FLOATTYPE)
            {
                FloatingDigitCount++;
            }
            else
            {
                DecimalCount++;
            }

            break;
        case '.':
            NumberType = FLOATTYPE;
            PeriodCount++;
            break;
        default:
            OtherChar++;
            isNum=false;
            break;
        }
        index++;
    }

    if (((((DecimalCount +FloatingDigitCount )== StrLength-1) || (DecimalCount +FloatingDigitCount) == StrLength)) && PeriodCount <=1 && OtherChar==0 )
    {
        if ((DecimalCount +FloatingDigitCount )>0)
        {
            isNum=true;

        }
        else
        {
            ErrorTracer(SYNTAX_ERROR,0,0);
        }

    }

    //std::cout << Number << std::endl;
    //PrintNumberType(NumberType);
    return isNum;
}

