/*
Name :  Lexer.cpp
Author: Amit Malyala, Copyright Amit Malyala, 2019. All rights reserved.
Description:
This module creates lexer tokens.
Notes:


To do:

Bug and revision history;
0.1 Initial version
*/
#include "Lexer.h"
#include "Stack.h"
#include <cstring>
// Comment when module is integrated with the project
// Uncomment to develop or test



/*
Component function: SCHAR8 Lexer::PeekNextChar(SCHAR8* str, std::size_t Length, std::size_t index)
Arguments:  char string, length, current index.
Returns: next char of index.
Description:  returns a char
Version : 0.1
*/
SCHAR8 Lexer::PeekNextChar(SCHAR8* str, std::size_t Length, std::size_t index)
{
	//std::cout << "in peek next char function " << std::endl;
	if ((index + 1) < Length)
	{
		return str[index + 1];
	}
	else
	{
		return ENDOFINPUTSTRING;
	}
}


/*
 Component Function: BOOL Lexer::IsWhiteSpace(SCHAR8 Character)
 Arguments:  Character
 Returns:
 Description:
 This function checks if a character is whitespace or not
  Version : 0.1
 */
BOOL Lexer::isWhitespace(SCHAR8 Character)
{
	return (Character == ' ' || Character == '\t');
}


/* Skip whitespace */
void Lexer::SkipWhitespace(SCHAR8* str, std::size_t Length, std::size_t& i)
{
	SCHAR8 ch; /* Use this variable outside of function?*/
	do
	{
		ch = PeekNextChar(str, Length, i);
		if (Lexer::isWhitespace(ch))
		{
			i++;
		}
	} while (Lexer::isWhitespace(ch));
}

/*
 Component Function: BOOL Lexer::isIdentChar(SCHAR8 Character)
 Arguments:  Character to be searched.
 Returns:
 Description:
 Returns is ident char or not.
  Version : 0.1
 */
BOOL Lexer::isIdentChar(SCHAR8 Character)
{
	return ((DataStructures::isAlphabet(Character)) || Character == '_' || Character == '.' || (DataStructures::isDigit(Character) == true));
}

/*
 Component Function: BOOL Lexer::isSeparator(SCHAR8 Character)
 Arguments:  Character to be searched in separators
 Returns:
 Description:
 Checks if a char is a separator.
  Version : 0.1
 */
BOOL Lexer::isSeparator(SCHAR8 Character)
{
	return (Character == ')' || Character == '(');
}
/*
 Component Function: BOOL Lexer::isblockSeparator(SCHAR8 Character)
 Arguments:  Character to be searched in separators
 Returns:
 Description:
 Checks if a char is a separator.
  Version : 0.1
 */
BOOL Lexer::isblockSeparator(SCHAR8 Character)
{
	return (Character == '}' || Character == '{');
}


/*
 Component Function: BOOL Lexer::isIdent(const SCHAR8* str)
 Arguments:  string to be checked.
 Returns:
 Description: This function looks up all characters of a string in a list of alphabets a-z and A-Z and digits
 Returns ident or not
 Version : 0.1
 */
BOOL Lexer::isIdent(const SCHAR8* str)
{
	std::size_t i = 0;
	std::size_t StrLength = DataStructures::strlength(str);
	std::size_t CharCount = 0;
	BOOL isIdent = false;
	std::size_t Alphabetcount = 0;
	while (i < StrLength)
	{
		if (isIdentChar(str[i]))
		{
			if (DataStructures::isAlphabet(str[i]))
			{
				Alphabetcount++;
			}
			CharCount++;
		}
		i++;
	}
	if ((CharCount == StrLength) && (StrLength > 0) && (Alphabetcount))
	{
		if (DataStructures::isAlphabet(str[0]) || str[0] == '_')
		{
			isIdent = true;
		}
		else
		{
			ErrorTracer(IDENTIFIER_STARTCHAR_ERROR, 0, 0);
		}
	}
	return isIdent;
}



/*
 Component Function: BOOL Lexer::isOperator(const SCHAR8* str)
 Arguments:  string to be checked.
 Returns:
 Description: This function checks if char string is a operator.
 Returns op or not
 Version : 0.1
 */
BOOL Lexer::isOperator(const SCHAR8* str)
{
	std::size_t i = 0;
	std::size_t StrLength = DataStructures::strlength(str);
	std::size_t CharCount = 0;
	BOOL isOp = false;
	while (i < StrLength)
	{
		if (Lexer::isOperchar(str[i]))
			CharCount++;
		i++;
	}

	if (CharCount == StrLength)
	{
		isOp = true;
	}
	return isOp;
}
/*
 Component Function: BOOL Lexer::isOperchar(SCHAR8 Ch)
 Arguments:  character
 Returns:
 Description:
 returns char is a operator or not
  Version : 0.1
 */
BOOL Lexer::isOperchar(SCHAR8 Ch)
{
	BOOL isoperchar = false;
	switch (Ch)
	{
	case '+':
	case '-':
	case '/':
	case '*':
	case '=':
	case '<':
	case '>':
	case '|':
	case '&':
	case '~':
	case '!':
	case '^':
	case ':':
	case ',':
		isoperchar = true;
		break;
	default:
		break;
	}
	return isoperchar;
}
/*
 Component Function: BOOL Lexer::isValidOp(std::size_t OpType)
 Arguments:  Op type
 returns: None
 Description:
 Detects Op type
 Version : 0.1
 Notes:
 */
BOOL Lexer::isValidOp(std::size_t OpType)
{
	if (OpType == NOTOP)
	{
		return false;
	}
	else
	{
		return true;
	}
}

/*
 Component Function: std::size_t Lexer::DetectOpType(SCHAR8 const* OpString)
 Arguments:  char string
 returns: None
 Description:
 Detects Op type
 Version : 0.1
 Notes:
 */
std::size_t Lexer::DetectOpType(SCHAR8 const* OpString)
{
	std::size_t ReturnOpType = NOTOP;
	if ((strcmp(OpString, "+") == 0))
	{
		ReturnOpType = OPPLUS;
	}
	else if ((strcmp(OpString, "-") == 0))
	{
		ReturnOpType = OPMINUS;
	}
	else if ((strcmp(OpString, "/") == 0))
	{
		ReturnOpType = OPDIV;
	}
	else if ((strcmp(OpString, "*") == 0))
	{
		ReturnOpType = OPMUL;
	}
	else if ((strcmp(OpString, "%") == 0))
	{
		ReturnOpType = OPMODULUS;
	}
	else if ((strcmp(OpString, "^") == 0))
	{
		ReturnOpType = OPPOWEXPONENT;
	}
	else if ((strcmp(OpString, "=") == 0))
	{
		ReturnOpType = OPEQUALTO;
	}
	else if ((strcmp(OpString, "+=") == 0))
	{
		ReturnOpType = OPADDEQUALTO;
	}
	else if ((strcmp(OpString, "-=") == 0))
	{
		ReturnOpType = OPMINUSEQUALTO;
	}
	else if ((strcmp(OpString, "*=") == 0))
	{
		ReturnOpType = OPMULTIPLYEQUALTO;
	}
	else if ((strcmp(OpString, "/=") == 0))
	{
		ReturnOpType = OPDIVEQUALTO;
	}
	else if ((strcmp((OpString), "sin") == 0))
	{
		ReturnOpType = OPSIN;
	}
	else if ((strcmp((OpString), "cos") == 0))
	{
		ReturnOpType = OPCOS;
	}
	else if ((strcmp((OpString), "tan") == 0))
	{
		ReturnOpType = OPTAN;
	}
	else if ((strcmp(OpString, "sec") == 0))
	{
		ReturnOpType = OPSEC;
	}
	else if ((strcmp(OpString, "cosec") == 0))
	{
		ReturnOpType = OPCOSEC;
	}
	else if ((strcmp(OpString, "cot") == 0))
	{
		ReturnOpType = OPCOT;
	}
	else if ((strcmp(OpString, "sqrt") == 0))
	{
		ReturnOpType = OPSQRT;
	}
	else if ((strcmp(OpString, "loge") == 0))
	{
		ReturnOpType = OPLOG;
	}
	else if ((strcmp(OpString, "<") == 0))
	{
		ReturnOpType = OPLESSTHAN;
	}
	else if ((strcmp(OpString, ">") == 0))
	{
		ReturnOpType = OPGREATERTHAN;
	}
	else if ((strcmp(OpString, "<<") == 0))
	{
		ReturnOpType = OPLEFTBITSHIFT;
	}
	else if ((strcmp(OpString, ">>") == 0))
	{
		ReturnOpType = OPRIGHTBITSHIFT;
	}
	else if ((strcmp(OpString, "!") == 0))
	{
		ReturnOpType = OPLOGICALNOT;
	}
	else if ((strcmp(OpString, "|") == 0))
	{
		ReturnOpType = OPBITWISEOR;
	}
	else if ((strcmp(OpString, "&") == 0))
	{
		ReturnOpType = OPBITWISEAND;
	}
	else if ((strcmp(OpString, "&&") == 0))
	{
		ReturnOpType = OPLOGICALAND;
	}
	else if ((strcmp(OpString, "||") == 0))
	{
		ReturnOpType = OPLOGICALOR;
	}
	else if ((strcmp(OpString, "<=") == 0))
	{
		ReturnOpType = OPLESSTHANEQUALTO;
	}
	else if ((strcmp(OpString, ">=") == 0))
	{
		ReturnOpType = OPGREATERTHANEQUALTO;
	}
	else if ((strcmp(OpString, "==") == 0))
	{
		ReturnOpType = OPLOGICALISEQUALTO;
	}
	else if ((strcmp(OpString, "!=") == 0))
	{
		ReturnOpType = OPLOGICALISNOTEQUALTO;
	}
	else if ((strcmp(OpString, "xor") == 0))
	{
		ReturnOpType = OPBITWISEXOR;
	}
	else if ((strcmp(OpString, "~") == 0))
	{
		ReturnOpType = OPCOMPLEMENT;
	}

	else if ((strcmp(OpString, "%=") == 0))
	{
		ReturnOpType = OPMODEQUALTO;
	}
	else if ((strcmp(OpString, "~=") == 0))
	{
		ReturnOpType = OPCOMPLEMENTEQUALTO;
	}
	else if ((strcmp(OpString, "_") == 0))
	{
		ReturnOpType = OPUNARYMINUS;
	}
	else if ((strcmp(OpString, "<<=") == 0))
	{
		ReturnOpType = OPLEFTSHIFTEQUALTO;
	}
	else if ((strcmp(OpString, ">>=") == 0))
	{
		ReturnOpType = OPRIGHTSHIFTEQUALTO;
	}
	else if ((strcmp(OpString, "&=") == 0))
	{
		ReturnOpType = OPANDEQUALTO;
	}
	else if ((strcmp(OpString, "|=") == 0))
	{
		ReturnOpType = OPOREQUALTO;
	}
	else if ((strcmp(OpString, "::") == 0))
	{
		ReturnOpType = OPSCOPERESOLLUTION;
	}
	else if ((strcmp(OpString, ",") == 0))
	{
		ReturnOpType = OPCOMMA;
	}
	//std::cout << "At endof DetectOpType() function " << std::endl;
	return ReturnOpType;
}






/*
 Component function: SCHAR8* Lexer::getTokencharstring(void)
 Arguments: char string
 returns : a substring of type SCHAR8*
 Version : 0.1
 Description:
 parses a char string into substrings.
 Each substring canbe these:
 ident,
 numeric,
 operator
 parenthesis ( )
 These substrings would be read by Tokenprocessor() function.

 Change funtion.

 Notes:
 Delete substring only in this module.
 Function being edited

 To do:
 Change this function to detect strings as names instead of idents, keywords etc.
 Detect a complete string under " "

 std::cout << "Hello, world!\n" << std::endl;
 should produce  tokens as these:
 In the above string \n is detected as two characters '\\' and 'n'

 std
 ::
 cout
 <<
 "
 Hello, world!\n
 "
 <<
 std
 ::
 endl
 ;

 Known issues:


To do:

Create a text parser which would separate tokens with ' ', ( ,), {,} ,'\n', ';' ,'"' tokens.
The only exception is " forms a separate token and characters between " " form a char string token.
a switch case statement would do this nicely.
*/
SCHAR8* Lexer::getTokencharstring(void)
{
	static SCHAR8* str = nullptr;
	static SCHAR8* substring = nullptr;
	BOOL IdentDetected = false;
	static std::size_t length = 0;
	//std::cout << "Length of char string " << length << std::endl;
	SCHAR8 ch = ' ';
	//create substring and send it to token processor which sends it to getNextToken() each time getNextToken() is called.
	std::size_t Count = 0;
	/*  separate counter for each type of substring */
	SCHAR8 NextChar = ' ';
	static std::size_t i = 0;
	BOOL substringdetected = false;
	static BOOL quotesdetected = false;
	if (i == length)
	{
		i = 0;
		str = io::getInput();
		if (str)
		{
			length = DataStructures::strlength(str);
		}
		else
		{
			length = 0;
		}
		//std::cout << "reached end of string while parsing"  << std::endl;
	}
	if (substring != nullptr)
	{
		//std::cout << "Substring is not empty, deleting it  "  << std::endl;
		delete[] substring;
		substring = nullptr;
	}
	if (str != nullptr && (length))
	{
		//std::cout << "allocating memory for newline and Substring."  << std::endl;
		substring = new SCHAR8[length + 1]; /* +2 for newline and line terminator for single char strings*/
		while (i < length && substringdetected == false)
		{
			ch = str[i];

			//#if(0)
			if (ch == '"')
			{
				if (quotesdetected)
				{
					quotesdetected = false;
				}
				else
				{
					quotesdetected = true;
				}
				Count = 0;
				substring[Count] = ch;
				substring[Count + 1] = '\0';
				substringdetected = true;
			}
			else if (quotesdetected)
			{
				Count = 0;
				do
				{
					ch = str[i];
					substring[Count] = ch;
					NextChar = Lexer::PeekNextChar(str, length, i);
					if (NextChar != '"')
					{
						i++;
						Count++;
					}
				} while (NextChar != '"' && NextChar != '\n');
				substring[Count + 1] = '\0';
				substringdetected = true;
				//quotesdetected=false;
			}

			//else
			//#endif

			//#endif
			else if (Lexer::isWhitespace(ch))
			{
				Count = 0;
				Lexer::SkipWhitespace(str, length, i);
			}
			else  if (ch == '\n')
			{
				Count = 0;
				//std::cout << "character is new line" << std::endl;
				/* Check if \n positoin is at length of char string.*/
				if (quotesdetected)
				{
					ErrorTracer(MISSING_QUOTES, 0, 0);
				}
				substring[Count] = ch;
				substring[Count + 1] = '\0';
				substringdetected = true;
			}
			else if ((DataStructures::isDigit(ch)) || ch == '.' || (Lexer::isIdentChar(ch)))
			{
				if (Count == 0)
				{
					if (ch == '_' || (DataStructures::isAlphabet(ch)))
					{
						IdentDetected = true;
					}

				}
				if (!IdentDetected)
				{

					/* Detect strings as 1a+ 3b . 1a should produce a error instead of tokens 1 and a */
					substring[Count] = ch;
					//std::cout << " before PeekNextchar()"  << std::endl;
					NextChar = Lexer::PeekNextChar(str, length, i);
					if (((!Lexer::isIdentChar(NextChar)) && NextChar != '.') || NextChar == '#')
					{
						//std::cout <<  "substring is ident " << std::endl;
						// Create end of substring here */
						substring[Count + 1] = '\0';
						substringdetected = true;
						IdentDetected = false;
						//std::cout << "Substring detected is " << substring << std::endl;
					}
					else
					{
						Count++;
					}
				}
				else
				{
					substring[Count] = ch;
					//std::cout << " before PeekNextchar()"  << std::endl;
					NextChar = Lexer::PeekNextChar(str, length, i);
					if ((!Lexer::isIdentChar(NextChar)) || NextChar == '#')
					{
						//std::cout <<  "substring is ident " << std::endl;
						// Create end of substring here */
						substring[Count + 1] = '\0';
						substringdetected = true;
						IdentDetected = false;
						//std::cout << "substring is " << substring << std::endl;
					}
					else
					{
						Count++;
					}
				}
			}
			/* a line terminator ; */
			else  if (ch == ';')
			{
				/* Detect nested ()  as (((1*4)*2)*3) */
				Count = 0;
				substring[Count] = ch;
				substring[Count + 1] = '\0';
				substringdetected = true;

			}
			/* Write code for these two tokens */
			else  if (Lexer::isSeparator(ch))
			{
				/* Detect nested ()  as (((1*4)*2)*3) */
				Count = 0;
				substring[Count] = ch;
				substring[Count + 1] = '\0';
				substringdetected = true;

			}
			/* Write code for these two tokens */
			else  if (Lexer::isblockSeparator(ch))
			{
				/* Detect nested { } */
				Count = 0;
				substring[Count] = ch;
				substring[Count + 1] = '\0';
				substringdetected = true;

			}

			else if (Lexer::isOperchar(ch))
			{
				substring[Count] = ch;
				/* Detect operators as ++, -- , +=,. *= , /=., a= !b, a=-b or a=+b.
				Detect a/=b, a*=b, a+=b, a%=b, a-=b. */
				NextChar = Lexer::PeekNextChar(str, length, i);
				if (!Lexer::isOperchar(NextChar))
				{
					// Create end of substring here */
					substring[Count + 1] = '\0';
					Count = 0;
					substringdetected = true;
				}
				else
				{
					Count++;
				}
			}

			else
			{
				substring[Count] = ch;
				substring[Count + 1] = '\0';
				substringdetected = true;
				ErrorTracer(ILLEGAL_CHARACTER, 0, 0);
			}

			i++;
		}
	}
	else
	{
		substring = nullptr;
	}
	//std::cout << "Substring at return statement " << substring << std::endl;
	//std::cout << "substring in getTokencharString: " << substring << std::endl;
	return substring; // This is appending \0 at the end of each string.
}

