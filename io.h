
/*
Name : Input.h
Author: Amit Malyala
Description:
Read a input ascii file into a series of strings.
Version: 0.1
*/

#ifndef INPUT_H
#define INPUT_H

#include <string>
#include <fstream>
#include <iostream>
#include "std_types.h"
#include "Error.h"
#include <vector>

namespace io
{
/*
Another solution:
This function would be called by getTokencharString() function;
*/
SCHAR8* getInput(void);


 
/*
 Component Function: void ReadFile(std::vector <SCHAR8> &s,const SCHAR8* Fileptr)
 Arguments:  Vector to process input and input file name.
 returns: None
 Description:
 Reads a ascii or UTF8 file into a vector.
 Version : 0.1
 Notes:
 From cplusplus.com
 */
void ReadFile(std::vector <SCHAR8> &s,const SCHAR8* Fileptr);

}

#endif